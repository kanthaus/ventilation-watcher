Ventilation device watcher
==========================

This repository contains a firmware for an ESP32 node designed to watch a central ventilation system (Systemair SAVE VTC 700 L) and log statistics from it into InfluxDB.

The firmware is written using ESPHome but does not require to have a running HomeAssistant instance.
Its configuration is stored in `ventilation.yaml`.

## Instructions

Install ESPHome:
```
python3 -m venv .venv
source .venv/bin/activate
pip install esphome
```

Create a `secrets.yaml` file with the credentials required by the node (which can be found in our password store):
```
device_password: "some random password that protects Over The Air (OTA) firmware updates"
wifi_password: "the password of the kh-iot network"
hotspot_password: "some random password for the hotspot created by the device if it fails to connect to the wifi network"
influxdb_auth: "the authentication header to pass to influxdb, in the form of 'Basic XXXXXX' where XXXXX is the encoding of the username and password"
```

For the initial set up, you need to connect the ESP32 to your computer to flash it. Once that initial set up is done, it will be able to use OTA updates. In both cases, updating the firmware is done with:
```
esphome run ventilation.yaml
```

## Physical setup

This firmware assumes a ESP32 node connected to [a RS485 module](https://www.makershop.de/module/kommunikation-module/ttl-rs485-adapter/) via a serial connection on pins 17 and 18 (see the configuration).

The RS485 module is connected to the ventilation unit via the 3-wire Modbus RTU interface.
 
## Debugging

You can access a web interface for the node at http://192.168.5.39/.
Another option to debug 

## Logging more quantities

See the [official variable list from the manufacturer](https://shop.systemair.com/upload/assets/SAVE_MODBUS_VARIABLE_LIST_20210301_REV36.PDF?a94f4fe0).

To add a new register to the node, update `ventilation.yaml` with the additional register. The address found in the documentation needs to be converted to hexadecimal and you need to substract 1 to account for the different numbering conventions.
The documentation indicates whether a register is signed (`I*`) or unsigned (`I`), which determines the value type to use: `S_WORD` or `U_WORD` respectively.

## Misc

CC0 license
